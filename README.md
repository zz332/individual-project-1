# Static Site with Zola and hosted on Vercel


## Demo Video of Website 
[Watch the video](https://www.youtube.com/watch?v=RMkHWxZ6UGY)

## Deploy on the Vercel
![](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG67.jpg)


## Purpose 
The purpose of this project is to create a static portfoilo website with Zola and host on the Vercel. I show my recent projects in my website.

## Website Preparation and Deployment with Zola and Vercel

Follow these steps to set up, test, and deploy your Zola website using Vercel, integrated with GitLab for continuous integration and deployment.

### Setting Up Your Zola Website

1. **Install Zola**
   - Download and install Zola. Visit the [Zola Installation Guide](https://www.getzola.org/documentation/getting-started/installation/) for detailed instructions.

2. **Initialize Your Zola Project**
   - Create a new site with Zola by running:
     ```
     zola init my-website
     ```
   - Replace `my-website` with the name of your project.

3. **Add a Theme**
   - Navigate to the themes directory:
     ```
     cd my-website/themes
     ```
   - Add your chosen theme as a Git submodule. Replace `<theme-git-url>` with the URL of the theme:
     ```
     git submodule add <theme-git-url>
     ```

4. **Create Content**
   - Create your web pages within the `content` folder of your Zola project.

5. **Local Testing**
   - Test your website locally using:
     ```
     zola serve
     ```
   - Or build the static files with:
     ```
     zola build
     ```

### Deploying with Vercel

6. **Install Vercel CLI**
   - Download and install the Vercel CLI tool.

7. **Login to Vercel**
   - Authenticate with Vercel using:
     ```
     vercel login
     ```

8. **Link Your Project to Vercel**
   - In your project's root directory, run:
     ```
     vercel link
     ```
   - Follow the prompts to link your repository to a Vercel project.

9. **Integrate Vercel with GitLab**
   - Navigate to your GitLab project's Settings > CI/CD > Variables section.
   - Add your Vercel token as a secret variable named `VERCEL_TOKEN`.

10. **Configure GitLab CI/CD**
    - In your GitLab repository, add a `.gitlab-ci.yml` file to define the CI/CD pipeline. This file should specify the steps for testing, building, and deploying your Zola website on Vercel.

### Continuous Deployment

- Push changes to your GitLab repository to trigger the CI/CD pipeline, automatically building and deploying your website through Vercel.


+++
title = "Thesis Publication"
date = 2022-07-26
weight = 3
+++
1. Thesis name is "Reliable induced L∞ control for polytopic uncertain continuous-time singular systems with dynamic quantization". My work is model-building and calculation: used LMI toolbox in MATLAB to solve sets of linear matrix inequalities to determine critical parameters for input quantization; drafted the modeling and calculation parts of the published paper.
[Project Link](https://ideas.repec.org/a/eee/apmaco/v443y2023ics0096300322008335.html)

2. Thesis name is "Effect of Particle Size and Morphology of Siliceous Supplementary Cementitious Material on the Hydration and Autogenous Shrinkage of Blended Cement". My work is digitizing photos of 100+ sets of siliceous tailing, identified their 2-dimensional outlines, and codified the roundness of these outlines. Then I Analyzed the statistical correlation between siliceous tailing roundness and cementhydration process through SPSS one-way ANOVA. 
[Project Link](https://www.mdpi.com/1996-1944/16/4/1638)
+++
title = "All other projects (keep updating)"
date = 2024-03-05
weight = 4
+++
1. Use R to do web scripting to get the data of sales of Amazon kindles. Then use the data to do the topic models, time series, sentiment analysis. I also made a shiny app and a dynamic topics model.

2. Write a python function that automatically tunes blackbox regression model. Input is a vector and output is a number. I use three regularization method: Droupout, NoiseAddition, and Robust. I used Monte Carlo for the first 2 methods and defined a column bounds for the third. Then I used CV-fold to test my model and evaluate them by MSE and MAD

3. Implementing all combinations for estimator (Lasso, Adptive Lasso, Ridge, Adaptive ridge) and tuning method (AIC, BIC, LOO-CV). Generating 1000 datasets with dense and sparse parameters from a generative model and fit different models to compute MSE. Averaging the MSE to obtain average error and finding patterns for dense and sparse signals


4. Loop over a image datasets and produce embeddings of the image. Then use logistic regression and random forest to classify the features
[Project Link](https://colab.research.google.com/drive/1ZiOmzJUgn0ICG3MNsRZxcgQ4vn6F19jr#scrollTo=ZtkwdOAgfQRu)


